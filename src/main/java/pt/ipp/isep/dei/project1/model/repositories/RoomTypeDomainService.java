package pt.ipp.isep.dei.project1.model.repositories;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.dei.project1.dto.housedto.RoomDto;
import pt.ipp.isep.dei.project1.dto.housedto.RoomTypeDto;
import pt.ipp.isep.dei.project1.mapper.MapperListRoomType;
import pt.ipp.isep.dei.project1.mapper.MapperRoom;
import pt.ipp.isep.dei.project1.model.house.Room;
import pt.ipp.isep.dei.project1.model.house.RoomType;

import java.util.Collections;
import java.util.List;

@Service
public class RoomTypeDomainService {


    @Getter
    private final List<RoomType> listOfRoomType;
    private final RoomTypeRepository roomTypeRepository;

    @Autowired
    public RoomTypeDomainService(RoomTypeRepository roomTypeRepository) {
        this.roomTypeRepository = roomTypeRepository;
        this.listOfRoomType = roomTypeRepository.findAll();
    }

    public RoomType getTypeByName(String name) {
        for (int roomTypeIndex = 0; roomTypeIndex < listOfRoomType.size(); roomTypeIndex++)
            if (listOfRoomType.get(roomTypeIndex).getType().equals(name))
                return listOfRoomType.get(roomTypeIndex);
        return null;
    }

    public RoomDto getRoomByTypeDto(String roomName) {
        for (RoomType room : listOfRooms)
            if (room.getName().equals(roomName))
                return MapperRoom.toDto(room);
        return null;
    }

    ////

    public boolean newRoomType(RoomTypeDto roomTypeDto) {
        RoomType room;
        String roomType;
        try {
            room = new RoomType(roomType);
        } catch (RuntimeException e) {
            return false;
        }
        return addRoom(room);
    }

    /////

    public static boolean add(RoomType type) {
        if (!listOfRoomType.contains(type)) {
            listOfRoomType.add(type);
            roomTypeRepository.save(type);
            return true;
        } else
            return false;
    }

    public List<RoomTypeDto> getListOfRoomTypesDTO() {
        if (!listOfRoomType.isEmpty())
            return MapperListRoomType.toDto(listOfRoomType).getListOfRoomTypeDto();
        return Collections.emptyList();
    }

}
