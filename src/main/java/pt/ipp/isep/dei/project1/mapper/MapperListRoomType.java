package pt.ipp.isep.dei.project1.mapper;

import pt.ipp.isep.dei.project1.dto.housedto.ListOfRoomsTypeDto;
import pt.ipp.isep.dei.project1.model.house.RoomType;

import java.util.List;

public class MapperListRoomType {


    protected MapperListRoomType(){
        throw new IllegalStateException("Utility class");
    }

    public static ListOfRoomsTypeDto toDto(List<RoomType> roomTypeList){

        ListOfRoomsTypeDto listOfRoomsTypeDto = new ListOfRoomsTypeDto();

        listOfRoomsTypeDto.setRoomDto (roomTypeList);

        return listOfRoomsTypeDto;

    }



}
