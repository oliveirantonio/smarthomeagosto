package pt.ipp.isep.dei.project1.controllersweb.house;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.dei.project1.dto.housedto.RoomDto;
import pt.ipp.isep.dei.project1.dto.housedto.RoomTypeDto;
import pt.ipp.isep.dei.project1.model.house.RoomType;
import pt.ipp.isep.dei.project1.model.repositories.RoomDomainService;
import pt.ipp.isep.dei.project1.model.repositories.RoomTypeDomainService;



@RestController
@RequestMapping(value = "/room-type-configuration")
@CrossOrigin(origins={"http://localhost:3000","http://smarthomeg1.azurewebsites.net"}, maxAge = 3600)
public class DefineNewRoomTypeWebController {


    private final RoomTypeDomainService roomTypeDomainService;

    @Autowired
    public DefineNewRoomTypeWebController(RoomTypeDomainService roomTypeDomainService) {
        this.roomTypeDomainService = roomTypeDomainService;
    }

    @PostMapping(value = "/new")
    public ResponseEntity<Object> createNewTypeRoom( @RequestBody RoomDto newRoomTypeDto) {
        boolean result = roomTypeDomainService.newRoomType(newRoomTypeDto);
        if (result)
            return new ResponseEntity<>(newRoomTypeDto.getName() + " added!", HttpStatus.CREATED);
        return new ResponseEntity<>("Impossible to create or add!", HttpStatus.CONFLICT);
    }




    /*


    private final RoomTypeDomainService roomTypeDomainService;

    @Autowired
    public DefineNewRoomTypeController(RoomTypeDomainService roomTypeDomainService) {
        this.roomTypeDomainService = roomTypeDomainService;
    }

    public boolean newRoomType(String type) {
        RoomType newType = new RoomType(type);
        return RoomTypeDomainService.add(newType);
    }

     */



}
