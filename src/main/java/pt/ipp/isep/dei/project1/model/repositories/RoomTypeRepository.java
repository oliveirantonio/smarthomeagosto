package pt.ipp.isep.dei.project1.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pt.ipp.isep.dei.project1.model.geographicarea.GeographicAreaType;
import pt.ipp.isep.dei.project1.model.house.RoomType;

import java.util.List;

@Repository
public interface RoomTypeRepository extends CrudRepository<RoomType,String> {

    List<RoomType> findAll();

}
