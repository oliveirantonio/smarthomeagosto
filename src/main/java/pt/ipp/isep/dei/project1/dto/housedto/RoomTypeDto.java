package pt.ipp.isep.dei.project1.dto.housedto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomTypeDto {


    private String type;

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof RoomDto))
            return false;
        RoomTypeDto c = (RoomTypeDto) obj;
        return this.type.equals(c.type);
    }

    @Override
    public int hashCode() {
        return this.type.charAt(0);
    }

}
