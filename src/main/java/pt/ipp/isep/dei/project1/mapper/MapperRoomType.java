package pt.ipp.isep.dei.project1.mapper;

import pt.ipp.isep.dei.project1.dto.housedto.RoomTypeDto;
import pt.ipp.isep.dei.project1.model.house.RoomType;

public class MapperRoomType {

    protected MapperRoomType(){ throw new IllegalStateException("Utility class"); }

    public static RoomTypeDto toDto (RoomType roomType){

        RoomTypeDto roomTypeDto = new RoomTypeDto();
        roomTypeDto.setType(roomType.getType());

        return roomTypeDto;

    }

}

