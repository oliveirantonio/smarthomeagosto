package pt.ipp.isep.dei.project1.model.house;


import lombok.Getter;
import lombok.Setter;
import pt.ipp.isep.dei.project1.model.sensor.SensorType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class RoomType {

    @Id
    @Column(name = "type")
    private String type;

    public RoomType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "\n Room Type: " + type;
    }

    @Override
    public int hashCode() {
        return this.type.charAt(0);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof RoomType))
            return false;
        RoomType c = (RoomType) obj;
        return (this.type.equals(c.getType()));

    }

    public boolean checkIfIsDesignation (RoomType roomType){
        return  (this.type.equalsIgnoreCase(roomType.type));
    }

}
