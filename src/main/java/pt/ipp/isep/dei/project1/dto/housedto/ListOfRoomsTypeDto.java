package pt.ipp.isep.dei.project1.dto.housedto;

import lombok.Getter;
import pt.ipp.isep.dei.project1.mapper.MapperRoomType;
import pt.ipp.isep.dei.project1.model.house.RoomType;
import java.util.ArrayList;
import java.util.List;

public class ListOfRoomsTypeDto {


    @Getter
    private final List<RoomTypeDto> listOfRoomTypeDto = new ArrayList<>();

    public void setRoomDto(List<RoomType> roomList) {
        for (RoomType roomType:roomList)
            listOfRoomTypeDto.add(MapperRoomType.toDto(roomType));
    }




    /*

    @Getter
    private final List<GeographicAreaTypeDto> listOfGATypesDto = new ArrayList<>();


    public void setListOfGATypesDTO(List<GeographicAreaType> mListOfGATypes) {
        for (GeographicAreaType geographicAreaType:mListOfGATypes)
            listOfGATypesDto.add(MapperGeographicAreaType.toDto(geographicAreaType));
    }

     */


}
