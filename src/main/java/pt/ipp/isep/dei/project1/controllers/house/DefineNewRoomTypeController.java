package pt.ipp.isep.dei.project1.controllers.house;

import org.springframework.beans.factory.annotation.Autowired;
import pt.ipp.isep.dei.project1.model.house.RoomType;
import pt.ipp.isep.dei.project1.model.repositories.RoomTypeDomainService;

public class DefineNewRoomTypeController {


    private final RoomTypeDomainService roomTypeDomainService;

    @Autowired
    public DefineNewRoomTypeController(RoomTypeDomainService roomTypeDomainService) {
        this.roomTypeDomainService = roomTypeDomainService;
    }

    public boolean newRoomType(String type) {
        RoomType newType = new RoomType(type);
        return RoomTypeDomainService.add(newType);
    }

}
